#ifndef DINITZ_H
#define DINITZ_H

typedef struct Node {
    int data;
    struct Node *next;
} Node;


/*@ predicate is_valid_linked_list(Node* head) =
  @   head == \null || (is_valid_linked_list(head->next) && \valid(head));
  @*/

/*@ requires \valid(head_ref) && (*head_ref) != \null;
  @ ensures *head_ref != \null && (*head_ref)->data == new_data && (*head_ref)->next == \old(*head_ref) && \fresh(*head_ref, sizeof(Node)) && \separated(*head_ref, \old(*head_ref));
  @ allocates *head_ref;
  @ assigns *head_ref, (*head_ref)->data, (*head_ref)->next;
  @*/
void insertHead(Node **head_ref, int new_data);


/*@ requires \valid(head_ref) && *head_ref != \null && is_valid_linked_list(*head_ref);
  @ assigns *head_ref;
  @ ensures \at(*head_ref,Post) == \old((*head_ref)->next) && !\valid(\old(*head_ref));
  @*/
void popHead(Node **head_ref);


/*@ requires start >= 0 && end >= 0 && end < 300000;
  @ requires graph != \null && \valid(levels + (0..end));
  @ requires \forall int i; 0 <= i < end + 1  ==> \valid(graph[i] + (0..end));
  @ assigns levels[0..end];
  @ ensures \result == 0 || \result == 1;
  @ ensures \forall int i; 0 <= i <= end ==> levels[i] >= -1 && levels[i] <= end;
  @ ensures \forall int i; 0 <= i <= end && levels[i] == -1 ==> (\forall int j; 0 <= j <= end && levels[j] != -1 ==> graph[j][i] == 0.0 && graph[i][j] == 0.0);
  @ ensures levels[end] > 0 ==> (\forall int i; \exists int j; \exists int k; 0 <= i < levels[end] && 0 <= j <= end && 0 <= k <= end && (levels[j] == i && levels[k] == i + 1) && graph[j][k] > 0);
  @*/
int BFS(int start, int end, double **graph, int *levels);


/*@ requires 0 <= cur <= end && end >= 0 && end < 300000;
  @ requires 0 <= flow <= DBL_MAX;
  @ requires graph != \null && \valid(levels + (0..end)) && \valid(visited + (0..end));
  @ requires \forall int i; 0 <= i <= end ==> \valid(graph[i] + (0..end));
  @ assigns visited[0..end], graph[0..end][0..end];
  @ ensures \result >= 0.0 && (\result > 0.0 ==> (\forall int i; 0 <= i <= end ==> graph[cur][i] <= \old(graph[cur][i])));
  @*/
double DFS(int cur, int end, double **graph, int *levels, int *visited, double flow);


/*@ axiomatic SumCapacity {
  @   logic double sum_capacity(double **graph, integer nodes, integer index) reads graph[0][0..nodes-1];
  @   axiom sum_capacity_base:
  @     \forall double **graph, integer nodes;
  @       0 <= nodes ==> sum_capacity(graph, nodes, 0) == 0;
  @   axiom sum_capacity_step:
  @     \forall double **graph, integer nodes, integer index;
  @       0 <= index < nodes - 1 ==>
  @         sum_capacity(graph, nodes, index + 1) == sum_capacity(graph, nodes, index) + graph[0][index];
  @ }
  @*/

/*@ predicate valid_flow(double result, double **graph, integer nodes) =
  @   result >= -1 && result <= sum_capacity(graph, nodes, nodes - 1);
  @*/

/*@ requires nodes > 0 && nodes <= 300000;
  @ requires graph != \null && \forall integer i; 0 <= i < nodes ==> \valid(graph[i] + (0..nodes-1));
  @ ensures valid_flow(\result, graph, nodes);
  @ assigns graph[0..nodes-1][0..nodes-1];
  @*/
double dinitzMaxFlow(int nodes, double **graph);


#endif // DINITZ_H
