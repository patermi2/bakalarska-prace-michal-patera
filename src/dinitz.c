#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <string.h>
#include "dinitz.h"

#define min(a, b) ((a) < (b) ? (a) : (b))


void insertHead(Node** head_ref, int new_data) {
    Node* next = (*head_ref);
    (*head_ref) =  (Node*) malloc(sizeof(Node));
    //@ assert \valid(head_ref);
    (*head_ref)->data = new_data;
    (*head_ref)->next = next;
}


void popHead(Node** head_ref) {
    Node* nextHead = (*head_ref)->next;
    /*@ assert \valid(*head_ref); */
    free(*head_ref);
    (*head_ref) = nextHead;
}


int BFS(int start, int end, double **graph, int *levels){
    /*@ loop invariant 0 <= i <= end + 1;
      @ loop invariant \forall int j; 0 <= j < i ==> levels[j] == -1;
      @ loop assigns i, levels[0..end];
      @ loop variant end - i;
      @*/
    for (int i = 0; i <= end; ++i) {
        levels[i] = -1;
    }
    levels[start] = 0;
    Node* queueHead = NULL;
    insertHead(&queueHead, start);
    int cur;
    /*@ loop invariant \valid(queueHead) || queueHead == \null;
      @ loop invariant \forall int i; 0 <= i <= end ==> levels[i] >= -1 && levels[i] <= end;
      @ loop assigns cur, queueHead, levels[0..end];
      @*/
    while(queueHead != NULL){
        cur = queueHead->data;
        popHead(&queueHead);
        /*@ loop invariant \forall int i; 0 <= i <= end + 1 ==> levels[i] >= -1 && levels[i] <= end;
          @ loop assigns queueHead, levels[0..end];
          @*/
        for (int i = 0; i <= end; ++i) {
            if(graph[cur][i] > 0){
                if(levels[i] < 0){
                    levels[i] = levels[cur] + 1;
                    insertHead(&queueHead, i);
                }
            }
        }
    }
    if (levels[end] < 0){
        return 0;
    }
    else {
        return 1;
    }
}


double DFS (int cur, int end, double **graph, int *levels, int *visited, double flow){
    if (cur == end){
        return flow;
    }
    visited[cur] = 1;
    /*@ loop invariant 0 <= i <= end;
      @ loop invariant 0 <= flow <= DBL_MAX;
      @ loop assigns i, graph[0..end][0..end], visited[0..end];
      @ loop variant end - i;
      @*/
    for (int i = 0; i <= end; ++i) {
        if (!visited[i] && graph[cur][i] > 0 && levels[i] == levels[cur] + 1){
            double augmentingFlow = DFS(i, end, graph, levels, visited, min(flow, graph[cur][i]));
            if (augmentingFlow > 0){
                graph[cur][i] -= augmentingFlow;
                graph[i][cur] += augmentingFlow;
                return augmentingFlow;
            }
        }
    }
    return 0;
}


double dinitzMaxFlow(int nodes, double **graph){
    double total = 0;
    int *vertexLevel = (int*) malloc(nodes * sizeof(int));
    int *visited;

    // Run BFS to check if flow is possible and set the levels of nodes
    /*@ loop invariant 0 <= total && total <= sum_capacity(graph, nodes, nodes - 1);
      @ loop assigns total, vertexLevel[0..nodes-1], *visited, graph[0..nodes-1][0..nodes-1];
      @ loop variant nodes - 1 - vertexLevel[nodes-1];
      @*/
    while (BFS(0, nodes - 1, graph, vertexLevel))
    {

        // Initialize visited array
        visited = (int*) calloc(nodes, sizeof(int));
        double flow;
        /*@ loop invariant 0 <= total;
          @ loop assigns flow, total, visited[0..nodes-1], graph[0..nodes-1][0..nodes-1];
          @ loop variant (int)(DBL_MAX - total) * 1000;
          @*/
        while (1)
        {
            flow = DFS(0, nodes - 1, graph, vertexLevel, visited, DBL_MAX);
            if (flow == 0.0)
            {
                break;
            }
            else
            {
                if (DBL_MAX - total < flow) {
                    total = DBL_MAX;
                    break;
                } else {
                    total += flow;
                }
            }
            // Reset visited array
            memset(visited, 0, nodes * sizeof(int));
        }
        free(visited);
    }
    free(vertexLevel);

    // Return max flow
    return total;
}
