#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <string.h>
#include "dinitz.h"



/*@ axiomatic MemoryManagement {
  @   logic boolean is_valid_graph(double **graph, integer nodes) reads graph[0][0..nodes-1];
  @   axiom valid_graph:
  @     \forall double **graph, integer nodes;
  @       0 <= nodes && nodes <= 300000 ==>
  @         is_valid_graph(graph, nodes) <==> (
  @           \valid(graph) &&
  @           \forall int i; 0 <= i < nodes ==> \valid(graph[i] + (0..nodes-1))
  @         );
  @ }
  @*/

/*@ requires \valid(graph);
  @ ensures (\result == -1 && *graph == \null) || (\result >= 2 && \result <= 300000 && is_valid_graph(*graph, \result));
  @ assigns *graph;
  @ allocates graph;
  @ allocates graph[0..\result-1];
  @*/
int readInput( double ***graph)
{
    int nodes;
    // Reading the amount of nodes and allocating
    printf("Insert the number of nodes.\n");
    if(scanf("%d", &nodes) != 1){
        printf("Invalid number of nodes.\n");
        return -1;
    }
    if (nodes < 2 || nodes > 300000){
        printf("Invalid number of nodes.\n");
        return -1;
    }

    *graph = (double**) malloc((nodes) * sizeof(double *));
    if (*graph == NULL){
        return -1;
    }
    /*@ loop invariant 0 <= i <= nodes;
      @ loop invariant \forall int j; 0 <= j < i ==> \valid((*graph)[j] + (0..nodes-1));
      @ loop assigns i, (*graph)[0..nodes-1];
      @ loop variant nodes - i;
      @*/
    for (int i = 0; i < (nodes); ++i)
    {
        (*graph)[i] = (double*) calloc((nodes), sizeof(double));
        if ((*graph)[i] == NULL){
            return -1;
        }
    }

    // Reading all the edges and saving them to the 2D array
    int start, end;
    double cap;
    printf("Insert the edges of the graph.\n");
    /*@ loop invariant 0 <= start < nodes;
      @ loop invariant 0 <= end < nodes;
      @ loop invariant 0 <= cap;
      @ loop assigns start, end, cap, graph[0..nodes-1][0..nodes-1];
      @*/
    while (scanf("%d %d %lf", &start, &end, &cap) == 3)
    {
        if (start < 0 || end < 0 || cap <= 0 || start >= (nodes) || end >= (nodes))
        {
            printf("This edge is invalid.\n");
        }
        else if ((*graph)[start][end] != 0.0)
        {
            printf("This edge already exists.\n");
        }
        else
        {
            (*graph)[start][end] = cap;
        }
    }
    return nodes;
}


/*@
  @ requires nodes > 0;
  @ requires \valid(graph + (0 .. nodes-1));
  @ requires \forall int i; 0 <= i < nodes ==> \valid(graph[i] + (0..nodes-1));
  @ ensures \forall integer i; 0 <= i < nodes ==> graph[i] == \null;
  @ ensures !\valid(graph) && (\forall int i; 0 <= i < nodes ==> !\valid(graph[i]));
  @ assigns graph[0..nodes-1];
  @*/
void cleanUp(int nodes, double** graph){
    /*@ loop invariant 0 <= i <= nodes;
      @ loop invariant \forall integer k; 0 <= k < i ==> graph[k] == \null;
      @ loop assigns i, graph[0..nodes-1];
      @ loop variant nodes - i;
      @*/
    for (int i = 0; i < nodes; i++){
        free(graph[i]);
        graph[i] = NULL;
    }
    free(graph);
}

/*@ ensures \result == 0 || \result == 1;
  @ assigns \nothing;
  @*/
int main()
{
    int nodes;
    double **graph;
    double total = -1;
    nodes = readInput(&graph);
    if(nodes == -1){
        return 1;
    }
    total = dinitzMaxFlow(nodes, graph);
    if (total == -1.0)
    {
        cleanUp(nodes, graph);
        return 1;
    }
    else
    {
        printf("The total flow of the graph is: %lf\n", total);
        cleanUp(nodes, graph);
    }
    return 0;
}
